# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140322191406) do

  create_table "accounts", force: true do |t|
    t.string   "username"
    t.string   "hashed_password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "experiments", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.integer  "simulator_id"
    t.text     "simulator_input"
    t.integer  "account_id"
    t.integer  "forked_from_id"
    t.text     "results"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "experiments", ["forked_from_id"], name: "index_experiments_on_forked_from_id"

  create_table "jobs", force: true do |t|
    t.integer  "experiment_id"
    t.integer  "simulator_id"
    t.string   "status"
    t.string   "kind"
    t.integer  "depends_on_id"
    t.string   "log_file"
    t.float    "elapsed_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "simulators", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.text     "description"
    t.text     "authors"
    t.string   "organization"
    t.string   "license"
    t.string   "website"
    t.string   "binary_path"
    t.string   "script_path"
    t.string   "local_path"
    t.boolean  "built"
    t.text     "input_schema"
    t.text     "output_schema"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "systems", force: true do |t|
    t.string "simulators_path"
    t.string "traces_path"
    t.string "jobs_path"
  end

end
