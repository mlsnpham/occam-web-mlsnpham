class CreateSystems < ActiveRecord::Migration
  def up
    create_table :systems do |t|
      t.string :simulators_path
      t.string :traces_path
      t.string :jobs_path
    end
  end

  def down
    drop_table :systems
  end
end
