class CreateExperiments < ActiveRecord::Migration
  def up
    create_table :experiments do |t|
      t.string  :name

      t.text    :tags

      t.integer :simulator_id
      t.text    :simulator_input

      t.integer :account_id
      t.integer :forked_from_id

      t.text    :results

      t.timestamps
    end

    add_index :experiments, :forked_from_id
  end

  def down
    drop_table :experiments
  end
end
