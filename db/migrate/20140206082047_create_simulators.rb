class CreateSimulators < ActiveRecord::Migration
  def up
    create_table :simulators do |t|
      t.string   :name
      t.text     :tags
      t.text     :description
      t.text     :authors
      t.string   :organization
      t.string   :license
      t.string   :website

      t.string   :binary_path
      t.string   :script_path

      t.string   :local_path
      t.boolean  :built

      t.text     :input_schema
      t.text     :output_schema

      t.timestamps
    end
  end

  def down
    drop_table :simulators
  end
end
