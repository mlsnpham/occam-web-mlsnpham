class CreateAccounts < ActiveRecord::Migration
  def up
    create_table :accounts do |t|
      t.string   :username
      t.string   :hashed_password

      t.timestamps
    end
  end

  def down
    drop_table :accounts
  end
end
