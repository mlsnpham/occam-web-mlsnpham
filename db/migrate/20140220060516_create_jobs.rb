class CreateJobs < ActiveRecord::Migration
  def up
    create_table :jobs do |t|
      t.integer :experiment_id
      t.integer :simulator_id

      t.string  :status
      t.string  :kind

      t.integer :depends_on_id

      t.string  :log_file
      t.float   :elapsed_time

      t.timestamps
    end
  end

  def down
    drop_table :jobs
  end
end
