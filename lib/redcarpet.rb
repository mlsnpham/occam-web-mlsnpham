module Redcarpet
  module Render
    class HTML
      require 'nokogiri'

      def codespan(code)
        "<code>#{CGI::escapeHTML(code).gsub(/\-/, "&#8209;")}</code>"
      end

      def image(link, title, alt_text)
        unless link.match /^http|^\//
          link = "/images/#{@slug}/#{link}"
        end

        options = alt_text.match(/^(.*)\|/)
        options = options[1] if options

        alt_text.gsub!(/^.*\|/, "")

        classes = "image"

        if options
          options.split('|').each do |option|
            case option
            when "border"
              classes << " border"
            when "right"
              classes << " right"
            when "left"
              classes << " left"
            end
          end
        end

        caption = ""
        caption = alt_text unless alt_text.start_with? "!"
        alt_text = Nokogiri::HTML(alt_text).xpath("//text()").remove

        img_source = "<img src='#{link}' title='#{title}' alt='#{alt_text}' />"

        if link.match "http[s]?://(www.)?youtube.com"
          # embed the youtube link
          youtube_hash = link.match("youtube.com/.*=(.*)$")[1]
          img_source = "<div class='youtube'><div class='youtube_fixture'><img src='/images/youtube_placeholder.png' /><iframe class='youtube_frame' src='http://www.youtube.com/embed/#{youtube_hash}'></iframe></div></div>"
        end

        caption = "<br /><div class='caption'>#{caption}</div>" unless caption == ""
        "</p><div class='#{classes}'>#{img_source}#{caption}</div><p>"
      end
    end
  end
end
