# Welcome to <span class="logo">Occam</span>

![border|!OCCAM provides architecture simulations on demand](/images/single-cpu-on-a-wafer.jpg)

OCCAM (Open Curation for Computer Architecture Modeling) is a project that will
serve as the catalyst for the tools, education, and community-building needed to
bring openness, accountability, comparability, and repeatability to computer
architecture experimentation.

[Developers](/simulators/new) will benefit by being able to easily distribute
their benchmarks.

[Experimentalists](/experiments/new) will be able to make use of developers'
work and rigorously evaluate their novel architecture capabilities.

## System
* [Configuration](/system)

## People

* [All](/accounts)
* [Sign up](/accounts/new)

## Simulators

* [All](/simulators)
* [Sign up](/simulators/new)

## Experiments

* [All](/experiments)
* [Sign up](/experiments/new)
