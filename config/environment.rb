require 'sinatra'
require 'sinatra/activerecord'

if ENV['RACK_ENV'] == "production"
  require 'pg'
else
  require 'sqlite3'
end

require_relative '../config/application'

class Occam < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  configure :test do
    ActiveRecord::Base.establish_connection(
      :adapter  => 'sqlite3',
      :database => ':memory:'
    )
  end

  configure :development do
    ActiveRecord::Base.establish_connection(
      :adapter  => 'sqlite3',
      :database => "#{File.expand_path(File.dirname(__FILE__))}/../dev.db",
      :timeout  => 1000
    )
  end

  configure :production do
    db = URI.parse('postgres://localhost/occam')

    ActiveRecord::Base.establish_connection(
      :adapter  => db.scheme == 'postgres' ? 'postgresql' : db.scheme,
      :host     => db.host,
      :user     => 'wilkie',
      :password => '',
      :database => db.path[1..-1],
      :encoding => 'utf8',
    )
  end
end
