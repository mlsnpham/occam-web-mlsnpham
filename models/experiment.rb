class Experiment < ActiveRecord::Base
  require 'json'

  # Fields

  # id        - Unique identifier.

  # name      - The name of the experiment.
  validates :name, :presence => true
  validates :name, :uniqueness => {
    :case_sensitive => false
  }

  # tags      - The semicolon separated list of tags.

  # simulator - The simulator to use.
  belongs_to :simulator
  validates  :simulator, :presence => true

  # simulator_input - The simulator configuration JSON.
  def simulator_input
    json = super
    if json.is_a? Hash
      json
    elsif json.is_a? String
      begin
        JSON.parse(super)
      rescue
        nil
      end
    end
  end

  def simulator_input=(value)
    if value.is_a? Hash
      value = value.to_json
    end

    super(value)
  end

  # account   - The account that created this experiment.
  belongs_to :account

  # forked_from - The experiment this one was based off of.
  belongs_to :forked_from, :class_name => "Experiment"

  def self.all_tags(with = "")
    self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
  end

  def self.all_tags_json(with = "")
    self.all_tags(with).to_json
  end

  def self.with_tag(tag)
    Experiment.where('tags LIKE ?', "%;#{tag};%")
  end

  def forked_us
    Experiment.where(:forked_from_id => self.id).select([:name, :id])
  end

  def self.search(term)
	Experiment.where('name LIKE ?', "%#{term}%").select([:name, :id])
  end
  
  def self.search_sim(name, tags, simulator, forked)
    query = Experiment.where('name LIKE ?', "%#{name}%")
    tags = tags.split ;
    tags.each do |tag|
      query = query.where('tags LIKE ?', "%#{tag}%")
    end
    if simulator > 0
      query = query.where('simulator_id = ?', "#{simulator}")
    end
   #query = query.where('account_id LIKE ?', "%#{account}%")
    if forked == 1
      query = query.where('forked_from_id IS NOT NULL')
    elsif forked == 2
      query = query.where('forked_from_id IS NULL')
    end
    query = query.select([:name, :id])
  end
  
  def self.search_adv(name, tags, simulator, forked, num_chan, jedec, tqd, cqd, epoch_length, rbp, ams, sched_pol, que_struct, tra, trans, command, addr_map, bus, bankstate, bank, power)
    query = Experiment.where('name LIKE ?', "%#{name}%")
    tags = tags.split ;
    tags.each do |tag|
      query = query.where('tags LIKE ?', "%#{tag}%")
    end
    if simulator > 0
      query = query.where('simulator_id = ?', "#{simulator}")
    end
   #query = query.where('account_id LIKE ?', "%#{account}%")
    if forked == 1
      query = query.where('forked_from_id IS NOT NULL')
    elsif forked == 2
      query = query.where('forked_from_id IS NULL')
    end
    if num_chan > 0
      query = query.where('simulator_input LIKE ?', "%num_chans\":\"#{num_chan}%")
    end
    if jedec > 0
      query = query.where('simulator_input LIKE ?', "%jedec_data_bus_bits\":\"#{jedec}%")
    end
    if tqd > 0
      query = query.where('simulator_input LIKE ?', "%trans_queue_depth\":\"#{tqd}%")
    end
    if cqd > 0
      query = query.where('simulator_input LIKE ?', "%cmd_queue_depth\":\"#{cqd}%")
    end
    if epoch_length > 0
      query = query.where('simulator_input LIKE ?', "%epoch_length\":\"#{epoch_length}%")
    end
    if rbp == 1
      query = query.where('simulator_input LIKE ?', "%row_buffer_policy\":\"close_page%")
    elsif rbp == 2
      query = query.where('simulator_input LIKE ?', "%row_buffer_policy\":\"open_page%")
    end
    if ams > 0
      query = query.where('simulator_input LIKE ?', "%address_mapping_scheme\":\"scheme#{ams}%")
    end
    if sched_pol == 1
      query = query.where('simulator_input LIKE ?', "%scheduling_policy\":\"rank_then_bank_round_robin%")
    elsif sched_pol == 2
      query = query.where('simulator_input LIKE ?', "%scheduling_policy\":\"bank_then_rank_round_robin%")
    end
    if que_struct == 1
      query = query.where('simulator_input LIKE ?', "%queuing_structure\":\"per_rank%")
    elsif que_struct == 2
      query = query.where('simulator_input LIKE ?', "%queuing_structure\":\"per_rank_per_bank%")
    end
    if tra > 0
      query = query.where('simulator_input LIKE ?', "%total_row_accesses\":\"#{tra}%")
    end
    if trans == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif trans == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if command == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif command == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if addr_map == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif addr_map == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if bus == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif bus == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if bankstate == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif bankstate == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if bank == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif bank == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    if power == 1
      query = query.where('simulator_input LIKE ?', "%#{}%")
    elsif power == 2
      query = query.where('simulator_input LIKE ?', "%#{}%")
    end
    query = query.select([:name, :id])
  end

  def initialize(options = {}, *args)
    # Ensure a leading and ending ',' in the tag CSV
    options[:tags] ||= ""
    if options[:tags].is_a? Array
      options[:tags] = ";#{options[:tags].join(';')};"
    end

    if !options[:tags].start_with?(";")
      options[:tags] = ";#{options[:tags]}"
    end
    if !options[:tags].end_with?(";")
      options[:tags] = "#{options[:tags]};"
    end

    super options, *args
  end
end
