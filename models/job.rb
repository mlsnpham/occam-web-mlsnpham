class Job < ActiveRecord::Base
  require 'json'

  # Fields

  # id         - Unique identifier.

  # experiment - The experiment to run.
  belongs_to :experiment

  # simulator  - The simulator to build.
  belongs_to :simulator

  # status     - The status of the job.
  validates_inclusion_of :status,
                         :in => [:unqueued,  :queued,  :pending,
                                 :suspended, :running, :finished]

  # kind       - The type of job.
  validates_inclusion_of :kind,
                         :in => [:build, :install, :run]

  # depends_on - The job that must be completed before this one can finish.
  has_one :job, :foreign_key => :depends_on_id

  # log_file   - The filename for the output of the job.

  def status
    status = super
    status.intern
  end

  def kind
    kind = super
    kind.intern
  end

  def initialize(options = {}, *args)
    # Ensure status,kind is interned, not a string
    # We trust Job class interns because they can only be created internally
    options[:status] = options[:status].intern if options[:status]
    options[:kind]   = options[:kind].intern   if options[:kind]

    super options, *args
  end
end
