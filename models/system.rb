class System < ActiveRecord::Base
  # Fields

  # id              - Unique identifier.

  # simulators_path - System path to simulator binaries

  # traces_path     - System path to traces

  # jobs_path       - System path for jobs

  # Retrieves the canonical system configuration
  def self.retrieve
    System.all.first
  end
end
