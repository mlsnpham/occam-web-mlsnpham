class Simulator < ActiveRecord::Base
  require 'json'
  require_relative '../lib/git'

  # Fields

  # id        - Unique identifier.

  # name      - The name of the experiment.
  validates :name, :presence => true
  validates :name, :uniqueness => {
    :case_sensitive => false
  }

  # tags      - The semicolon separated list of tags.

  def self.names
    self.select(:name, :id)
  end

  def self.all_tags(with = "")
    self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
  end

  def self.all_tags_json(with = "")
    self.all_tags(with).to_json
  end

  def self.with_tag(tag)
    Simulator.where('tags LIKE ?', "%;#{tag};%")
  end

  def self.search(term)
    Simulator.where('name LIKE ?', "%#{term}%").select([:name, :id])
  end
  
  def self.search_adv(name, tags, description, website, organization, license, authors)
    query = Simulator.where('name LIKE ?', "%#{name}%")
    tags = tags.split ;
    tags.each do |tag|
      query = query.where('tags LIKE ?', "%#{tag}%")
    end
    query = query.where('description LIKE ?', "%#{description}")
    query = query.where('website LIKE ?', "%#{website}")
    query = query.where('organization LIKE ?', "%#{organization}")
    query = query.where('license LIKE ?', "%#{license}")
    query = query.where('authors LIKE ?', "%#{authors}")
    query = query.select([:name, :id])
  end

  # Install the simulator to the system (delayed via worker)
  def install
    job = Job.find_by(:simulator_id => self.id, :kind => "install")

    if job.nil?
      job = Job.create(:simulator => self,
                       :kind      => "install",
                       :status    => "queued")
    end

    job
  end

  # Build the simulator (delayed via worker)
  def build
    install_job = Job.where(:simulator_id => self.id, :kind => "install").where.not(:status => "finished").first
    job         = Job.where(:simulator_id => self.id, :kind => "build").first

    if job.nil?
      job = Job.create(:simulator     => self,
                       :kind          => "build",
                       :status        => "queued",
                       :depends_on_id => install_job && install_job.id)
    end

    job
  end

  # Pull simulator record information from the given url in script_path
  def import
    json = Occam::Git.description(self.script_path)

    self.name         = json["name"]                     if json.has_key? "name"
    self.binary_path  = json["git"]                      if json.has_key? "git"
    self.license      = json["license"]                  if json.has_key? "license"
    self.organization = json["organization"]             if json.has_key? "organization"
    self.description  = json["description"]              if json.has_key? "description"
    self.website      = json["website"]                  if json.has_key? "website"
    self.authors      = ";#{json["authors"].join(';')};" if json.has_key? "authors"
    self.tags         = ";#{json["tags"].join(';')};"    if json.has_key? "tags"
  end

  def initialize(options = {}, *args)
    system = System.retrieve

    # Ensure a leading and ending ',' in the tag CSV
    options[:tags] ||= ""
    if options[:tags].is_a? Array
      options[:tags] = ";#{options[:tags].join(';')};"
    end

    if !options[:tags].start_with?(";")
      options[:tags] = ";#{options[:tags]}"
    end
    if !options[:tags].end_with?(";")
      options[:tags] = "#{options[:tags]};"
    end

    # Ensure a leading and ending ',' in the author CSV
    options[:authors] ||= ""
    if !options[:authors].start_with?(";")
      options[:authors] = ";#{options[:authors]}"
    end
    if !options[:authors].end_with?(";")
      options[:authors] = "#{options[:authors]};"
    end

    # Figure out the slug path to install the simulator to
    if options[:name]
      slug = options[:name].to_url
      root_path  = system.simulators_path
      options[:local_path] ||= "#{root_path}/#{slug}"
    end

    super options, *args
  end

  # Override to_a to split tag strings to arrays
  def to_hash
    hash = self.attributes

    hash["tags"] = hash["tags"].split(';').drop(1)
    hash["authors"] = hash["authors"].split(';').drop(1)

    hash
  end

  def to_json(*args)
    self.to_hash.to_json
  end
end
