class Account < ActiveRecord::Base
  require 'bcrypt'

  # Fields

  # id              - Unique identifier.

  # username        - The name used to log in.
  validates :username, :presence => true
  validates :username, :length => {
    :in => 4..30
  }
  validates :username, :uniqueness => {
    :case_sensitive => false
  }

  # hashed_password - The hash of the password.
  validates :hashed_password, :presence => {
    :message => "Password cannot be blank"
  }
  validates :hashed_password, :length => {
    :in => 60..60
  }

  # Create a hash of the password.
  def self.hash_password(password)
    BCrypt::Password.create(password, :cost => Occam::BCRYPT_ROUNDS)
  end

  # Determine if the given password matches the account.
  def authenticated?(password)
    BCrypt::Password.new(hashed_password) == password
  end

  def initialize(options = {}, *args)
    options.delete :hashed_password

    # Hash the password before entering it in our database
    if !(options[:password].nil? || options[:password] == "")
      options[:hashed_password] = Account.hash_password(options[:password])
    end

    options.delete :password

    super options, *args
  end
end
