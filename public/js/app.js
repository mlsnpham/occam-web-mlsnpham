// Autocomplete for experiment tags
$(document).ready(function(){
  var tag_cache = {};
  tag_cache["bl"] = ["foo", "bar"];
  $('input#authors').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input#tags').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input#tags').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
          return;
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  // Set up div expands
  $('.configuration-group span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated description div
    if ($(this).hasClass('shown')) {
      $(this).parent().find('.description').css({
        display: 'block'
      });
      $(this).text("[ - ]");
    }
    else {
      $(this).parent().find('.description').css({
        display: 'none'
      });
      $(this).text("[+]");
    }
  });

  // add prettyprint class to all <pre><code></code></pre> blocks
  var prettify = false;
  $("pre code").parent().each(function() {
    $(this).addClass('prettyprint');
    prettify = true;
  });

  // if code blocks were found, bring in the prettifier ...
  if ( prettify ) {
    $.getScript("/js/prettify.js", function() { prettyPrint() });
  }
});
