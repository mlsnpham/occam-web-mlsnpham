$(document).ready(function() {
  $('#results-data ul').each(function() {
    var maxKeyWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).children('span.key').width();
    }).get());

    if (maxKeyWidth > 0) {
      $(this).children('li').each(function() {
        $(this).children('span.key').each(function() {
          if ($(this).parent().children('span.value').children('ul.array').length == 0) {
            $(this).css({
              display: "inline-block",
              width: ""+maxKeyWidth+"px",
            });
          }
        });
      });
    }
  });

  $('#results-data ul').each(function() {
    var maxWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).width();
    }).get());

    $(this).children('li').each(function() {
      $(this).css({
        width: ""+maxWidth+"px",
      });
    });
  });

  $('li span.key').each(function() {
    var parent_ul = $(this).parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    $(this).on('click', function(e) {
      $(this).toggleClass("in-results");

      var key = $(this).data('key');

      if ($(this).hasClass("in-results")) {
        if (grandparent_ul.hasClass("array")) {
          // Highlight all
          grandparent_ul.children('li.element')
                        .children('ul')
                        .children('li')
                        .children('span.key').each(function() {
            if ($(this).data('key') == key) {
              $(this).addClass("in-results");
              $(this).css("color", "red");
            }
          });
          $(this).css("color", "red");
        }
        else {
          $(this).css("color", "blue");
        }
      }
      else {
        // Unhighlight all
        grandparent_ul.children('li.element')
                      .children('ul')
                      .children('li')
                      .children('span.key').each(function() {
          if ($(this).data('key') == key) {
            $(this).removeClass("in-results");
            $(this).css("color", "");
          }
        });
        $(this).css("color", "");
      }

      e.preventDefault();
    });
  });

  // Collapse Arrays
  $('#results-data ul.hash > li > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('span.value').children('ul').css({
        display: 'inline-block'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('span.value').children('ul').css({
        display: 'none'
      });
      $(this).text("\u25b8");
    }
  });

  fake_li = $('<li class="fake"><span class="key">...</span></li>').css({
    display: 'none'
  });

  $(this).find('#results-data ul.array > li.element > ul').append(fake_li);

  // Collapse array element hashes
  $('#results-data ul.array > li.element > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array element div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('ul').children('li').css({
        display: 'inline-block'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'none'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('ul').children('li').css({
        display: 'none'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'inline-block'
      });
      $(this).text("\u25b8");
    }
  });

  $('#results-data ul.hash > li > span.expand').trigger('click');
  $('#results-data > ul.hash > li > span.expand').trigger('click');
});
