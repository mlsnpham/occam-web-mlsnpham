require_relative 'helper'

describe Occam do
  describe "Session Controller" do
    describe "GET /login" do
      it "should render the login form" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"sessions/login",
          anything
        )

        get '/login'
      end
    end

    describe "GET /logout" do
      it "should delete the account_id session key" do
        session = stub('session')
        session.expects(:[]=).with(:account_id, nil)
        Occam.any_instance.stubs(:session).returns(session)

        get '/logout'
      end
    end

    describe "POST /login" do
      it "should post 302 when login is successful" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        session = stub('session')
        session.stubs(:[]=).with(:account_id, account.id)
        Occam.any_instance.stubs(:session).returns(session)

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 302
      end

      it "should set the account_id in the session when login is successful" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        session = stub('session')
        session.expects(:[]=).with(:account_id, account.id)
        Occam.any_instance.stubs(:session).returns(session)

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end

      it "should redirect to home when login is successful" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        session = stub('session')
        session.stubs(:[]=).with(:account_id, account.id)
        Occam.any_instance.stubs(:session).returns(session)

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.location.must_equal "http://example.org/"
      end

      it "should post 422 when login is unsuccessful" do
        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 422
      end

      it "should report an error when login is unsuccessful" do
        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:errors => Array)
        )

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end

      it "should render the login form when login is unsuccessful" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"sessions/login",
          anything
        )

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end
    end
  end
end
