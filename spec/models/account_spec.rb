require_relative "helper"
require_model "account"

describe Account do
  it "should not allow creation with an existing username" do
    Account.create(:username => "wilkie",
                   :password => "foo")

    account = Account.create(:username => "wilkie",
                             :password => "foo")

    account.errors.count.wont_equal 0
  end

  it "should not allow a blank password" do
    account = Account.create(:username => "wilkie",
                             :password => "")

    account.errors.count.wont_equal 0
  end

  it "should create an account when given a username and password" do
    account = Account.create(:username => "wilkie",
                             :password => "foobar")

    account.errors.count.must_equal 0
  end
end
