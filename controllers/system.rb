class Occam
  # Get the system configuration
  get '/system' do
    system = System.all.first
    render :haml, :"system/show", :locals => {
      :system => system
    }
  end

  # Update system configuration
  get '/system/new' do
    system = System.all.first
    system = System.new if system.nil?

    render :haml, :"system/new", :locals => {
      :system => system,
      :errors => system.errors
    }
  end

  # Persist system configuration
  post '/system' do
    system = System.all.first

    columns = {
      :simulators_path => params["simulators_path"],
      :traces_path     => params["traces_path"],
      :jobs_path       => params["jobs_path"],
    }

    if system.nil?
      system = System.create columns
    else
      system.update_columns columns
      system.save
    end

    if system.errors.any?
      status 422
      render :haml, :"system/new", :locals => {
        :system => system,
        :errors => system.errors
      }
    else
      redirect "/system"
    end
  end
end
