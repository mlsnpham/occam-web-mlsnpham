class Occam
  # Get a list of all experiments
  get '/experiments' do
    if params["tag"]
      experiments = Experiment.with_tag params["tag"]
    else
      experiments = Experiment.all
    end

    render :haml, :"experiments/index", :locals => {
      :experiments => experiments,
      :tags        => Experiment.all_tags,
      :tag         => params["tag"]
    }
  end

  # Create a new experiment
  post '/experiments' do
    simulator = Simulator.find_by(:id => params["simulator"].to_i)
    if params["forked_from"]
      forked_from = Experiment.find_by(:id => params["forked_from"].to_i)
    end

    experiment = Experiment.create :name        => params["name"],
                                   :tags        => params["tags"],
                                   :account     => current_account,
                                   :forked_from => forked_from,
                                   :simulator   => simulator

    # TODO: check validations
    if experiment.errors.any?
      status 422
      render :haml, :"experiments/new", :locals => {
        :errors => experiment.errors,
        :forked_from => nil,
        :simulators => Simulator.names,
        :experiment => experiment
      }
    else
     redirect "/experiments/#{experiment.id}"
    end
  end

  # Form to create a new experiment
  get '/experiments/new' do
    render :haml, :"experiments/new", :locals => {
      :errors => nil,
      :forked_from => nil,
      :experiment => Experiment.new,
      :simulators => Simulator.names
    }
  end

  # Retrieve a list of tags for autocomplete
  get '/experiments/tags' do
    Experiment.all_tags_json(params["term"])
  end

  # Fork experiment
  get '/experiments/:id/fork' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      simulator  = experiment.simulator

      render :haml, :"experiments/new", :locals => {
        :errors      => nil,
        :forked_from => experiment,
        :account     => current_account,
        :experiment  => experiment,
        :simulators  => Simulator.names
      }
    end
  end

  post '/experiments/:id/configuration' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    simulator  = experiment.simulator

    schema = JSON.parse(simulator.input_schema)

    experiment.simulator_input = params["data"]
    experiment.save

    redirect "/experiments/#{experiment.id}"
  end

  get '/experiments/:id/configuration.json' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    content_type "application/json"
    JSON.pretty_generate(experiment.simulator_input)
  end

  get '/experiments/:id/configuration' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    simulator  = experiment.simulator

    schema = JSON.parse(simulator.input_schema)

    render :haml, :"simulators/configure", :locals => {
      :schema     => schema,
      :simulator  => simulator,
      :data       => experiment.simulator_input,
      :experiment => experiment,
    }
  end

  # Retrieve a specific experiment
  get '/experiments/:id' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    job = Job.find_by(:experiment_id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      simulator  = experiment.simulator

      render :haml, :"experiments/show", :locals => {
        :experiment  => experiment,
        :forked_from => experiment.forked_from,
        :forked_us   => experiment.forked_us,
        :account     => experiment.account,
        :job         => job,
        :simulator   => simulator
      }
    end
  end

  # Retrieve the results page for this experiment
  get '/experiments/:id/results' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      simulator  = experiment.simulator

      output = JSON.parse(experiment.results)

      render :haml, :"results/show", :locals => {
        :experiment  => experiment,
        :schema      => JSON.parse(simulator.output_schema),
        :data        => output["data"] || {},
        :errors      => output["errors"] || [],
        :warnings    => output["warnings"] || [],
      }
    end
  end

  # Run the experiment
  get '/experiments/:id/run' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    job = Job.find_by(:experiment_id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      if job.nil?
        # Add something to our worker collection
        job = Job.create(:experiment => experiment,
                         :kind       => "run",
                         :status     => "queued")
      end

      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/experiments/#{experiment.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end

  # Cancel the experiment
  get '/experiments/:id/cancel' do
    job = Job.find_by(:experiment_id => params[:id].to_i)

    if job.nil?
      status 404
    else
      # Kill the process and remove from our worker collection
      job.destroy

      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/experiments/#{job.experiment_id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end
end
