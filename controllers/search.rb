class Occam
  get '/search' do
    simulators = Simulator.search params["q"]
    experiments = Experiment.search params["q"]

    [
      {
        :title => "Simulators",
        :results => simulators.map do |simulator|
		      ["#{simulator.id}", "#{simulator.name}"]
		    end
       
      },
      {
        :title => "Experiments",
        :results => experiments.map do |experiment|
		      ["#{experiment.id}", "#{experiment.name}"]
		    end
      }
    ].to_json
  end
  
  post '/search' do
    query = params["q"]
	simulators = Simulator.search params[:search]
	experiments = Experiment.search params[:search]
	
	render :haml, :"search/results", :locals => {
	  :simulators => simulators,
	  :experiments => experiments
	}
  end
  
  get '/search/advanced' do
    render :haml, :"search/index", :locals => {
	  :simulators => Simulator.names
	}
  end
  
  post '/search/advanced' do
    tags = params["tags"]
    tags_exp = params["tags_exp"]
    tags_exp << ";"
    tags_sim = params["tags_sim"]
    tags_sim << ";"
    tags_exp.concat tags
    tags_sim.concat tags
  
    simulators = Simulator.search_adv   params["name"],
                                        tags_sim,
                                        params["description"],
                                        params["website"],
                                        params["organization"],
                                        params["license"],
                                        params["authors"]
                                        
	experiments = Experiment.search_sim params["name"],
                                        tags_exp,
                                        params["sims"].to_i,
                                        params["forked"].to_i
    
    render :haml, :"search/results", :locals => {
      :simulators => simulators,
      :experiments => experiments
	}
  end
  
  get '/search/simulators' do
    render :haml, :"search/simulators", :locals => {
	  :simulators => Simulator.names
	}
  end
  
  post '/search/simulators' do
    simulators = Simulator.search_adv   params["name"],
                                        params["tags"],
                                        params["description"],
                                        params["website"],
                                        params["organization"],
                                        params["license"],
                                        params["authors"]
                                        
    experiments = Array.new
    
    render :haml, :"search/results", :locals => {
      :simulators => simulators,
      :experiments => experiments
	}
  end
  
  get '/search/experiments' do
    render :haml, :"search/experiments", :locals => {
	  :simulators => Simulator.names
	}
  end
  
  post '/search/experiments' do
  
	experiments = Experiment.search_adv params["name"],
                                        params["tags"],
                                        params["sims"].to_i,
                                        params["forked"].to_i,
                                        params["num_chan"].to_i,
                                        params["jedec"].to_i,
                                        params["tqd"].to_i,
                                        params["cqd"].to_i,
                                        params["epoch_length"].to_i,
                                        params["rbp"].to_i,
                                        params["ams"].to_i,
                                        params["sched_pol"].to_i,
                                        params["que_struct"].to_i,
                                        params["tra"].to_i,
                                        params["trans"].to_i,
                                        params["command"].to_i,
                                        params["addr_map"].to_i,
                                        params["bus"].to_i,
                                        params["bankstate"].to_i,
                                        params["bank"].to_i,
                                        params["power"].to_i
  
    simulators = Array.new
    
    render :haml, :"search/results", :locals => {
      :simulators => simulators,
      :experiments => experiments
	}
  end

  # Get a search result
  get '/search/results' do
    query = params["q"]
    simulators = Simulator.search params["q"]
    experiments = Experiment.search params["q"]
  end
end
