class Occam
  # Get a list of all accounts
  get '/accounts' do
    accounts = Account.all
    render :haml, :"accounts/index", :locals => {
      :accounts => accounts
    }
  end

  # Create a new account
  post '/accounts' do
    account = Account.create :username => params["username"],
                             :password => params["password"]

    # TODO: check validations
    if account.errors.any?
      status 422
      render :haml, :"accounts/new", :locals => {:errors => account.errors}
    else
      redirect "/accounts/#{account.id}"
    end
  end

  # Form to create a new account
  get '/accounts/new' do
    render :haml, :"accounts/new", :locals => {:errors => nil}
  end

  # Retrieve a specific account page
  get '/accounts/:id' do
    account = Account.find_by(:id => params[:id].to_i)
    experiments = Experiment.where(:account_id => params[:id].to_i)
    jobs = Job.where(:experiment_id => experiments.map(&:id))

    if account.nil?
      status 404
    else
      render :haml, :"accounts/show", :locals => {
        :account     => account,
        :experiments => experiments,
        :jobs        => jobs,
      }
    end
  end
end
