class Occam
  require 'json'

  get '/simulators/:id/configure.json' do
    simulator = Simulator.find_by(:id => params[:id].to_i)

    content_type "application/json"
    return simulator.input_schema
  end

  get '/simulators/:id/configuration' do
    simulator = Simulator.find_by(:id => params[:id].to_i)

    render :haml, :"simulators/configuration", :locals => {
      :simulator => simulator,
      :schema    => JSON.parse(simulator.input_schema),
    }
  end

  # Get a list of all simulators
  get '/simulators' do
    if params["tag"]
      simulators = Simulator.with_tag params["tag"]
    else
      simulators = Simulator.all
    end

    render :haml, :"simulators/index", :locals => {
      :simulators => simulators,
      :tags       => Simulator.all_tags,
      :tag        => params["tag"]
    }
  end

  # Create a new simulator
  post '/simulators' do
    if params.has_key? "import"
      simulator = Simulator.new :name         => params["name"],
                                :binary_path  => params["binary_path"],
                                :script_path  => params["script_path"],
                                :description  => params["description"],
                                :authors      => params["authors"],
                                :website      => params["website"],
                                :organization => params["organization"],
                                :license      => params["license"],
                                :tags         => params["tags"]
      simulator.import

      return render :haml, :"simulators/new", :locals => {
        :errors => simulator.errors,
        :simulator => simulator
      }
    end

    simulator = Simulator.create :name         => params["name"],
                                 :binary_path  => params["binary_path"],
                                 :script_path  => params["script_path"],
                                 :description  => params["description"],
                                 :authors      => params["authors"],
                                 :organization => params["organization"],
                                 :website      => params["website"],
                                 :license      => params["license"],
                                 :tags         => params["tags"]

    if simulator.errors.any?
      status 422
      render :haml, :"simulators/new", :locals => {
        :errors => simulator.errors,
        :simulator => simulator
      }
    else
      simulator.install
      simulator.build

      redirect "/simulators/#{simulator.id}"
    end
  end

  # Form to create a new simulator
  get '/simulators/new' do
    render :haml, :"simulators/new", :locals => {
      :errors => nil,
      :simulator => Simulator.new
    }
  end

  # Retrieve a list of tags for autocomplete
  get '/simulators/tags' do
    Simulator.all_tags_json(params["term"])
  end

  # Retrieve a specific simulator json
  get '/simulators/:id.json' do
    simulator = Simulator.find_by(:id => params[:id].to_i)
    job = Job.find_by(:simulator_id => params[:id].to_i)

    if simulator.nil?
      status 404
    else
      content_type "application/json"
      simulator.to_json
    end
  end

  # Retrieve a specific simulator
  get '/simulators/:id' do
    simulator   = Simulator.find_by(:id => params[:id].to_i)
    job         = Job.find_by(:simulator_id => params[:id].to_i, :kind => "build")
    install_job = Job.find_by(:simulator_id => params[:id].to_i, :kind => "install")

    if simulator.nil?
      status 404
    else
      render :haml, :"simulators/show", :locals => {
        :simulator   => simulator,
        :install_job => install_job,
        :job         => job,
      }
    end
  end

  # Start a simulator install
  get '/simulators/:id/install' do
    simulator = Simulator.find_by(:id => params[:id].to_i)

    if simulator.nil?
      status 404
    else
      simulator.install

      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/simulators/#{simulator.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end

  # Start a simulator build
  get '/simulators/:id/build' do
    simulator = Simulator.find_by(:id => params[:id].to_i)

    if simulator.nil?
      status 404
    else
      simulator.build

      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/simulators/#{simulator.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end
end
