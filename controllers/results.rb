class Occam
  require 'json'

  # Get a result
  get '/results' do
    json = JSON.parse(IO.read("public/js/results.json"))
    schema = JSON.parse(IO.read("public/js/schema.json"))
    render :haml, :"results/show", :locals => {
      :warnings => json["warnings"],
      :errors   => json["errors"],
      :data     => json["data"],
      :schema   => schema,
    }
  end
end
