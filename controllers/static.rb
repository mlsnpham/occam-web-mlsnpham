class Occam
  get '/' do
    markdown :index
  end

  get '/styleguide' do
    haml :styleguide
  end

  get '/about' do
    markdown :about
  end
end
