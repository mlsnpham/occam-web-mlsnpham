class Occam
  module ConfiguratorHelpers
    def render_config(schema, key=nil)
      if schema.is_a? Hash
        if schema.has_key? "type"
          # Output form input
          value = ""
          if schema.has_key?("default")
            value = schema["default"]
          end

          case schema["type"]
          when Array
            "<p class='#{schema["type"]}'>#{schema["label"]}</p>" +
            "<select>" + schema["type"].map { |type|
              selected = (value == type)
              "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
            }.join("") + "</select>" +
            "<div class='description'>#{schema["description"]}</div>"
          else
            "<p class='#{schema["type"]}'>#{schema["label"]}</p><p>#{value}</p>" +
            "<div class='description'>#{schema["description"]}</div>"
          end
        else
          # Output group
          "<ul class='configuration-group'>" + schema.map { |k,v|
            if v.is_a? Hash
              if v.has_key? "type"
                # An input value
                "<li>#{render_config(v, k)}</li>"
              else
                # A group
                "<li><h2>#{k}</h2>#{render_config(v, k)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end

    def render_form(hash, schema, key=nil)
      if schema.is_a? Hash
        if schema.has_key? "type"
          # Output form input
          value = ""
          if schema.has_key?("default")
            value = schema["default"]
          end
          if hash && hash.has_key?(key)
            value = hash[key]
          end

          description_div = ""
          if schema.has_key? "description"
            description_div = "<div class='description'>#{render(:markdown, schema["description"])}</div>"
          end

          case schema["type"]
          when "int", "float", "string"
            "<label class='#{schema["type"]}'>#{schema["label"]}</label> <span class='expand'>[+]</span><input name='data[#{key}]' value='#{value}'>" +
            description_div
          when "boolean"
            "<label class='boolean'>#{schema["label"]}</label> <span class='expand'>[+]</span><input type='checkbox' name='data[#{key}]'>" +
            description_div
          when Array
            "<label>#{schema["label"]}</label> <span class='expand'>[+]</span>" +
            "<select name='data[#{key}]'>" + schema["type"].map { |type|
              selected = (value == type)
              "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
            }.join("") + "</select>" +
            description_div
          else
          end
        else
          # Output group
          "<ul class='configuration-group'>" + schema.map { |k,v|
            if v.is_a? Hash
              if v.has_key? "type"
                # An input value
                "<li>#{render_form(hash, v, k)}</li>"
              else
                # A group
                "<li><h2>#{k}</h2>#{render_form(hash, v, k)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end
  end

  helpers ConfiguratorHelpers
end
