class Occam
  module SessionHelpers
    def current_account
      if session[:account_id]
        @current_account ||= Account.find_by(:id => session[:account_id])
      end
    end
  end

  helpers SessionHelpers
end
