class Occam
  module ResultsHelpers
    def render_data(hash, schema)
      if hash.is_a? Hash
        keys = hash.keys
        keys_array = keys.select{|k| hash[k].is_a?(Array) }
        keys_else  = keys.select{|k| !hash[k].is_a?(Array) }

        "<ul class='hash'>" + keys_else.sort.map{|k|
          v = hash[k]
          inner_schema = schema
          inner_schema = schema[k] if schema.has_key?(k)
          if inner_schema.is_a?(Hash) && inner_schema.has_key?("units")
            units = inner_schema["units"]
          end

          "<li>" +
          "<span class='key' data-key='#{k.to_s}'>#{k.to_s}</span>" +
          "<span class='value'#{" data-units='#{units}'" if units}>#{render_data(v, inner_schema)} #{units}</span></li>"
        }.join("") + keys_array.sort.map{ |k|
          v = hash[k]
          inner_schema = schema
          inner_schema = schema[k] if schema.has_key?(k)
          if inner_schema.is_a?(Hash) && inner_schema.has_key?("units")
            units = inner_schema["units"]
          end

          "<li>" +
          "<span class='expand shown'>&#9662;</span>" +
          "<span class='key' data-key='#{k.to_s}'>#{k.to_s}</span>" +
          "<span class='value'#{" data-units='#{units}'" if units}>#{render_data(v, inner_schema)} #{units}</span></li>"
        }.join("") + "</ul>"
      elsif hash.is_a? Array
        if schema.is_a? Array
          schema = schema.first
        end
        "<ul class='array'>" + hash.map{ |e|
          "<li class='element'>" +
          "<span class='expand shown'>&#9662;</span>" +
          "#{render_data(e, schema)}</li>"
        }.join("") + "</ul>"
      else
        return hash.to_s
      end
    end
  end

  helpers ResultsHelpers
end
